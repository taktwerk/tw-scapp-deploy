<?php

/*{{{ v.150906.001 (0.0.1)

	Bitbucket webhook interface.

	Based on 'Automated git deployment' script by Jonathan Nicoal:
	http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/

	See README.md and CONFIG.php

	---
	Igor Lilliputten
	mailto: igor at lilliputten dot ru
	http://lilliputtem.ru/

}}}*/

$scapp_env = __DIR__.'/../../.scapp/env.php';
if(file_exists($scapp_env)) {
    require_once ($scapp_env);
}

// Initalize:
require_once('log.php');
require_once('bitbucket.php');

// Load config:
include('CONFIG.php');

// make hook accessible from command line
if (!isset($_SERVER["HTTP_HOST"]) || $_GET['manual_push']) {
    $payload_json = '{ "push":{ "changes":[ { "commits":[ { "type":"commit" } ], 
        "new":{ "name":"' . $ENV_VARS['REPO_BRANCH'] . '" } } ] }, "repository":{ "name":"' . $ENV_VARS['REPO_NAME'] . '" } } ';
    $_POST['payload'] = json_decode($payload_json);
}

// Let's go:
initLog(); // Initalize log variables
initPayload(); // Get posted data
fetchParams(); // Get parameters from bitbucket payload (REPO)
checkPaths(); // Check repository and project paths; create them if neccessary
placeVerboseInfo(); // Place verbose log information if specified in config
fetchRepository(); // Fetch or clone repository
checkoutProject(); // Checkout project into target folder

// log POST variable
file_put_contents('postlog.log',print_r($_REQUEST,true));

