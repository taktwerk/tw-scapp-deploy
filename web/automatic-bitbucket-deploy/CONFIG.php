<?php

/*{{{ v.151005.001 (0.0.2)

	Sample config file for bitbucket hooks.

	Based on 'Automated git deployment' script by Jonathan Nicoal:
	http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/

	See README.md and CONFIG.php

	---
	Igor Lilliputten
	mailto: igor at lilliputten dot ru
	http://lilliputtem.ru/

}}}*/

/*{{{ Auxiliary variables, used only for constructing $CONFIG and $PROJECTS  */

$HOME_PATH = '/home/vcap/';
$REPOSITORIES_PATH = $HOME_PATH.'.repositories/';
$PROJECTS_PATH = $HOME_PATH;

// read config ini (workaround as PHP7 does not ignore #)
$string = file_get_contents($HOME_PATH . 'app/.env-scapp');
$array = explode("\n",$string);
foreach($array as $arr) {
	if(strpos($arr, '#') !== false)
		$output[] = strstr($arr, '#', true);
	else
		$output[] = $arr;
}
$ENV_VARS = parse_ini_string(implode("\n",$output));
//$ENV_VARS = parse_ini_file($HOME_PATH . 'app/.env-scapp');

/*}}}*/

// Base tool configuration:
$CONFIG = array(
	'bitbucketUsername' => 'taktwerk', // User name on bitbucket.org, *REQUIRED*
	'gitCommand' => 'git', // Git command, *REQUIRED*
	'repositoriesPath' => $REPOSITORIES_PATH, // Folder containing all repositories, *REQUIRED*
	'log' => true, // Enable logging, optional
	'logFile' => $HOME_PATH . 'app/bitbucket.log', // Logging file name, optional
	// 'logClear' => true, // clear log each time, optional
	'verbose' => true, // show debug info in log, optional
	// 'folderMode' => 0700, // creating folder mode, optional
);

// List of deploying projects:
$PROJECTS = array(
	// 'repo-name' => array( // The key is a bitbucket.org repository name
	// 	'projPath' => $PROJECTS_PATH.'deploy_path/', // Path to deploy project, *REQUIRED*
	// 	// 'postHookCmd' => 'your_command', // command to execute after deploy, optional
	// 	// 'branch' => 'master', // Deploing branch, optional
	// ),
	$ENV_VARS['REPO_NAME'] => array(
		'branch' => $ENV_VARS['REPO_BRANCH'],
		// 'projPath' => $PROJECTS_PATH.'work/',
		'projPath' => $PROJECTS_PATH.'app/',
		'postHookCmd' => 'sh ./.scapp/post-update.sh',
	),
);


