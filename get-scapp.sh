#!/bin/bash

# COPY THIS FILE TO ROOT OF DEPLOYABLE PROJECT

# download files
wget https://bitbucket.org/taktwerk/tw-scapp-deploy/get/master.zip -O tw-scapp-deploy.zip --no-cache

# copy file
unzip tw-scapp-deploy.zip
rm taktwerk-tw-scapp-deploy-*/.gitignore
rm taktwerk-tw-scapp-deploy-*/README.md
cp -r taktwerk-tw-scapp-deploy-*/. ./

# cleanup files
rm tw-scapp-deploy.zip
rm -rf taktwerk-tw-scapp-deploy-*

# run install
sh .scapp/install-scapp.sh