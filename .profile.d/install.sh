#!/bin/bash
sleep 2

echo -n "Installing ssh keys..."
touch installcmd.txt
mkdir ./../.ssh
mv ./.profile.d/ssh/id_rsa ./../.ssh/id_rsa
mv ./.profile.d/ssh/id_rsa.pub ./../.ssh/id_rsa.pub
mv ./.profile.d/ssh/known_hosts ./../.ssh/known_hosts
chmod 600 ./../.ssh/id_rsa
chmod 600 ./../.ssh/id_rsa.pub
chmod 644 ./../.ssh/known_hosts
chmod 700 ./../.ssh

echo -n " done!"

echo -n " Cloning git..."

# startup deploy
./php/bin/php ./web/automatic-bitbucket-deploy/bitbucket-hook.php

echo -n " done!"
