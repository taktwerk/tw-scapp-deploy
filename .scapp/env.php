<?php

// export VCAP SERVICES
$vcap = getenv('VCAP_SERVICES');
if ($vcap) {
    $vcap = json_decode($vcap, true);

    if ($vcap['mariadbent'] && $vcap['mariadbent'][0] && $vcap['mariadbent'][0]['credentials']) {
        //Export to Env variables "SCAPP_MYSQL_HOST", "SCAPP_MYSQL_PORT", "SCAPP_MYSQL_USERNAME", "SCAPP_MYSQL_PASSWORD", "SCAPP_MYSQL_DATABASE","SCAPP_MYSQL_NAME"
        foreach ($vcap['mariadbent'][0]['credentials'] as $key => $value) {
            $key = 'SCAPP_MYSQL_' . strtoupper($key);
            if (function_exists('apache_getenv') && function_exists('apache_setenv') && apache_getenv($key)) {
                apache_setenv($key, $value);
            }

            if (function_exists('putenv')) {
                putenv("$key=$value");
            }
            $_ENV[$key] = $value;
            $_SERVER[$key] = $value;
        }
    }
}
