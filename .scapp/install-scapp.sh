#!/bin/bash

# run installation
echo "run installation..."

cp .scapp/get-scapp.sh get-scapp.sh

echo "";
echo "> merge .env-dist with .env-scapp-dist settings to .env-scapp and adjust:"
echo "";
echo "> copy manifest.yml-dist to manifest.yml and adjust:"
echo "cp manifest.yml-dist manifest.yml"
echo "";
echo "> copy .bp-config/options.json-dist to .bp-config/options.json"
echo "cp .bp-config/options.json-dist .bp-config/options.json"
echo "";
echo "> create mysql service & key, get credentials for setting .env-scapp, host is 10.0.20.18:"
echo "cf create-service mariadbent usage tw-mariadb-projectname-dev"
echo "cf create-service-key tw-mariadb-projectname-dev mykey"
echo "cf service-key tw-mariadb-projectname-dev mykey"
echo "";
echo "> deploy app:"
echo "cf push"

