#!/bin/bash

# run installation
echo "run post update..."

cp .env-scapp .env;
./php/bin/php php/bin/composer.phar install --no-interaction --no-dev > composer.log;
./php/bin/php yii migrate --interactive=0;
./php/bin/php yii app/renew-version
./php/bin/php yii translate/export-models
./php/bin/php yii app/version

# create assets folder
mkdir -p web/assets
mkdir -p web/assets-prod
