taktwerk Scapp Deploy
============

Base package to deploy a starter-kit app to the Swisscom Application Cloud. Cloudfoundry credentials and cf console commands are needed to deploy. Commands are tested with mac environment only.


Installation
------------


Download and run package in the root folder of a starter-kit project

    wget https://bitbucket.org/taktwerk/tw-scapp-deploy/raw/master/.scapp/get-scapp.sh
    sh ./get-scapp.sh

follow the instructions in the terminal


> merge .env-dist with .env-scapp-dist settings to .env-scapp and adjust:

> copy manifest.yml-dist to manifest.yml and adjust:
    
    cp manifest.yml-dist manifest.yml

> copy .bp-config/options.json-dist to .bp-config/options.json
    
    cp .bp-config/options.json-dist .bp-config/options.json

> create mysql service & key, get credentials for setting .env-scapp, host is 10.0.20.18:
    
    cf create-service mariadbent usage tw-mariadb-projectname-dev
    cf create-service-key tw-mariadb-projectname-dev mykey
    cf service-key tw-mariadb-projectname-dev mykey

> deploy app:
    
    cf push